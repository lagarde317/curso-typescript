"use strict";
// function saludar(nombre:string){
//     console.log("hola" + nombre.toUpperCase);
// }
// var wolverine = {
//     nombre:"logan"
// };
// -------------------------------------------------TEMPLATE STRINGS-------------------------------------------------
// let nombre:string = "Alejandro";
// let apellido:string = "Lagarde";
// let edad:number = 20;
// let cadena:any = `Hola ${nombre} ${apellido} que tienen la edad de ${edad}`
// console.log(cadena);
// -------------------------------------------------FUNCIONES DE FLECHA-----------------------------------------------
// let miFuncion = function(a:any){
//     return a;
// }
// let miFuncion2 = (a:any) => a;
// let getNombre = function (nombre:string, apellido:string, apellido2:string){
//     return nombre+apellido+apellido2;
// }
// let getNombre2 = (nombre:string,apellido:string,apellido2:string) => nombre+apellido+apellido2;
// console.log(getNombre("Ale","lagarde","villaverde"));
// console.log(getNombre2("KARLA","martinez","cruz"));
// let nombreMayusculas = function(nombre:string) {
//     nombre = nombre.toUpperCase();
//     return nombre;
// }
// let nombreMayusculas2 = (nombre:string) => nombre.toUpperCase();
// console.log(nombreMayusculas("alejandro"));
// console.log(nombreMayusculas2("alejandro"));
// ---------------------------------------FUNCIONES DENTRO DE OBJETOS-----------------------------------------------------
// let hulk = {
//     nombre: "Hulk",
//     smash(){
//         setTimeout ( () => console.log(this.nombre + "smashhh!!"),1500);
//     }
// }
// hulk.smash();
// ---------------------------------------DESTRUCTURACIÓN DE OBJETOS Y ARREGLOS-------------------------------------------
// let avengers = {
//     tony: "Iron man",
//     steve: "Capitan america",
//     thor: "Dios del trueno"
// }
// let {tony,steve,thor} = avengers;
// console.log(tony,steve,thor);
// let avengers:string[]=["Salto","Thor","Ironman", "Capitan"];
// let [s,dios,capi,acero] = avengers;
// console.log(s,dios,capi,acero);
// ----------------------------------------------------------------PROMESAS-------------------------------------------------
// let promesa = new Promise(function(resolve,reject){
//     setTimeout(()=>{
//         console.log("Promesa terminada");
//         // resolve();
//         reject();
//     },1000)
// });
// promesa.then(function(){
//     console.log("Terminó bien");
//     },
//     function(){
//         console.error("Algo salió mal");
//     }
// );
// ------------------------------------------------INTERFACES DE TYPESCRIPT--------------------------------------------------
// interface Xmen{
//     nombre:string,
//     poder:string
// };
// let wolverine:Xmen ={
//     nombre: "Logan",
//     poder: "Regenerarse"
// }
// function enviarMision(xmen:Xmen){
//     return console.log("Enviado a "+ xmen.nombre + " a una misión");
// }
// function regresarCuartel(xemn:Xmen){
//     return console.log("Regresando a "+xemn.nombre+ " al cuartel");
// }
// enviarMision(wolverine);
// regresarCuartel(wolverine);
